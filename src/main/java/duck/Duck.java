package duck;

import duck.behaviorals.Fly;
import duck.behaviorals.Quack;

public abstract class Duck {

    private final Quack quack;
    private final Fly fly;
    private final String name;

    public Duck(Quack quack, Fly fly, String name) {
        this.quack = quack;
        this.fly = fly;
        this.name = name;
    }

    public void quack() {
        System.out.println(this.getName() +" say:");
        quack.quack();
    }

    public void fly() {
        System.out.println(this.getName() +" fly:");
        fly.fly();
    }

    public Quack getQuack() {
        return quack;
    }

    public Fly getFly() {
        return fly;
    }

    public String getName() {
        return name;
    }
}
