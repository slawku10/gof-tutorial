package duck.behaviorals.impl;

import duck.behaviorals.Fly;

public class FlyingWings implements Fly {

    public void fly() {
        System.out.println("I fly because i have wings !!!");
    }
}
