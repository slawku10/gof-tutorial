package duck.behaviorals.impl;

import duck.behaviorals.Quack;

public class LiveQuack implements Quack {

    public void quack() {
        System.out.println("quack, quack");
    }
}
