package duck.behaviorals.impl;

import duck.behaviorals.Quack;

public class Squeak implements Quack {

    @Override
    public void quack() {
        System.out.println("Squeak, Squeak");
    }
}
