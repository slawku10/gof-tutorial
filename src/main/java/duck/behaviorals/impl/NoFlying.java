package duck.behaviorals.impl;

import duck.behaviorals.Fly;

public class NoFlying implements Fly {

    @Override
    public void fly() {
        System.out.println("I'm not fly");
    }
}
