package duck.implDuck;

import duck.Duck;
import duck.behaviorals.impl.NoFlying;
import duck.behaviorals.impl.Squeak;

public class RubberDuck extends Duck {

    public RubberDuck() {
        super(new Squeak(), new NoFlying(), "Rubber Duck");
    }
}
