package duck.implDuck;

import duck.Duck;
import duck.behaviorals.impl.LiveQuack;
import duck.behaviorals.impl.FlyingWings;

public class WildDuck extends Duck {

    public WildDuck() {
        super(new LiveQuack(), new FlyingWings(), "Wild Duck");
    }
}
