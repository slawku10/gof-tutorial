import duck.Duck;
import duck.implDuck.RubberDuck;
import duck.implDuck.WildDuck;

import java.util.List;

public class StrategyRunner {

    public static void main(String[] args) {
        List.of(new WildDuck(),
                new RubberDuck())
                .forEach(d -> {
                    d.quack();
                    d.fly();
                });
    }
}
