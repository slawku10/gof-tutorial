import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class StrategyRunnerTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final StrategyRunner strategyRunner = new StrategyRunner();

    @BeforeEach
    public void setUp() {
        System.setOut(new PrintStream(outContent));
    }

    @AfterEach
    public void restore(){
        System.setOut(originalOut);
    }

    @Test
    void strategyRunnerOutputTest(){
        final String [] args = {};
        StrategyRunner.main(args);
        assertEquals( "Wild Duck say:\r\n" +
                "quack, quack\r\n" +
                "Wild Duck fly:\r\n" +
                "I fly because i have wings !!!\r\n" +
                "Rubber Duck say:\r\n" +
                "Squeak, Squeak\r\n" +
                "Rubber Duck fly:\r\n" +
                "I'm not fly\r\n", outContent.toString());
    }
}